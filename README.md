## Quick Summary ##
This project shows one way to add Vue to MVC Razor pages. For this strategy, we reference Vue in the _Layout page and instantiate our Vue instance there.
In order to make use of Vue on a page, we add to an array of mixins that is declared in the top level site.js file, as demonstrated in Index.cshtml.

This came from the article [here](https://www.giftoasis.com/blog/asp-net-core/vue/using-vue-with-asp-net-razor-can-be-great)

## How do I get set up? ##

* git clone, build and run
